package db;

import model.Indexable;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.assertEquals;

public class DummyDatabaseImplTest {

    static DummyDatabase db;

    @BeforeClass
    public static void setUp() {
        db = DummyDatabaseImpl.instance;
    }

    @Test
    public void createAndGetTable() throws DummyDatabaseException {
        String tableName = "table";
        final long entityId = 4298;
        db.addTable(tableName, new ConcurrentHashMap<Long, Indexable>() {{
            put(entityId, new Indexable() {
                @Override
                public long getId() {
                    return entityId;
                }

                @Override
                public String getEntityName() {
                    return "Entity";
                }
            });
        }});

        assertEquals(entityId, db.getTable(tableName).get(entityId).getId());
    }

    @Test(expected = DummyDatabaseException.class)
    public void oneTableCannotBeCreatedSeveralTimes() throws DummyDatabaseException {
        db.addTable("someTable", new ConcurrentHashMap<>());
        db.addTable("someTable", new ConcurrentHashMap<>());
    }

    @Test(expected = DummyDatabaseException.class)
    public void cannotToGetNonexistentTable() throws DummyDatabaseException {
        db.getTable("nonexistent");
    }
}