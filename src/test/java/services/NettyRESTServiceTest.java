package services;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class NettyRESTServiceTest {

    CloseableHttpClient client;
    CloseableHttpResponse response;
    NettyRESTService restService;

    @Before
    public void setUp() {
        client = HttpClients.createMinimal();
        restService = new NettyRESTService(8080, new RequestMapperImpl());
        new Thread(restService).start();
        restService.awaitStartService();
    }

    @After
    public void setDown() {
        try {
            if (client != null) client.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        try {
            if (response != null) {
                response.close();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        try {
            if (restService != null) restService.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void echoRequest() throws IOException {
        String echoMessage = "Some echo message";

        HttpPost post = new HttpPost("http://localhost:8080/echo");
        post.setEntity(new StringEntity(echoMessage));
        response = client.execute(post);

        HttpEntity entity = response.getEntity();
        assertNotNull(entity);
        assertEquals(echoMessage, IOUtils.toString(entity.getContent()));
    }

}