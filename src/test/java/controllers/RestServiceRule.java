package controllers;

import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.rules.ExternalResource;
import services.NettyRESTService;

import java.io.IOException;

public class RestServiceRule extends ExternalResource {
    CloseableHttpClient client;
    NettyRESTService restService;

    public RestServiceRule(CloseableHttpClient client, NettyRESTService restService) {
        this.client = client;
        this.restService = restService;
    }

    @Override
    protected void before() {
        new Thread(restService).start();
        restService.awaitStartService();
    }

    @Override
    protected void after() {
        try {
            if (client != null) client.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        try {
            if (restService != null) restService.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
