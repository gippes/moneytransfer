package controllers;

import com.google.gson.Gson;
import controllers.TransactionController.Response;
import db.DummyDatabaseImpl;
import model.Account;
import model.Transaction;
import org.junit.Before;
import org.junit.Test;
import repository.AccountRepository;
import repository.RepositoryException;
import repository.TransactionRepository;

import java.io.IOException;
import java.math.BigDecimal;

import static model.Transaction.Status.PROCESS;
import static org.junit.Assert.*;

public class TransactionControllerTest extends ControllerTest {
    Gson gson = new Gson();
    Account acc1 = new Account(1, new BigDecimal("100"));
    Account acc2 = new Account(2, new BigDecimal("30"));

    @Before
    public void setUp() {
        AccountRepository accountRepository = new AccountRepository(DummyDatabaseImpl.instance);
        try {
            accountRepository.add(acc1);
        } catch (RepositoryException ignored) {
        }
        try {
            accountRepository.add(acc2);
        } catch (RepositoryException ignored) {
        }

    }

    @Test
    public void executeTransaction() throws Exception {
        Transaction tr = new Transaction(1, BigDecimal.TEN, acc1.getId(), acc2.getId());
        Response response = gson.fromJson(requestPost("/transaction/execute", gson.toJson(tr, Transaction.class)), Response.class);

        assertNotNull(response);
        assertEquals(0, response.code);
        assertEquals(PROCESS, response.transaction.status);
        assertEquals("transaction id:1 executed successful", response.message);
        assertEquals(acc1.getBalance(), BigDecimal.valueOf(90));
        assertEquals(acc2.getBalance(), BigDecimal.valueOf(40));
    }

    @Test
    public void transactionNonExistentAccount() throws Exception {
        Transaction tr = new Transaction(2, BigDecimal.ZERO, acc1.getId(), 3);
        Response response = gson.fromJson(requestPost("/transaction/execute", gson.toJson(tr, Transaction.class)), Response.class);

        assertNotNull(response);
        assertEquals(-1, response.code);
        assertNull(response.transaction);
        assertEquals("account not found", response.message);
    }

    @Test
    public void getTransactionById() throws IOException, RepositoryException {
        Transaction tr = new Transaction(5, BigDecimal.ZERO, acc1.getId(), 3);
        new TransactionRepository(DummyDatabaseImpl.instance).add(tr);

        Response response = gson.fromJson(requestGet("/transaction/get?id=5"), Response.class);

        assertNotNull(response);
        assertEquals(0, response.code);
        assertEquals("transaction id:5 info", response.message);
    }
}