package controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import db.DummyDatabaseImpl;
import model.Account;
import org.junit.Test;
import repository.AccountRepository;
import repository.RepositoryException;

import java.io.IOException;

import static controllers.AccountController.Response;
import static org.junit.Assert.*;

public class AccountControllerTest extends ControllerTest {
    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

    @Test
    public void createAccount() throws Exception {
        Account account = new Account(101);
        String data = requestPost("/account/create", gson.toJson(account, Account.class));
        Response response = gson.fromJson(data, Response.class);

        assertNotNull(response);
        assertEquals(0, response.code);
        assertEquals("account id:101 created", response.message);
    }

    @Test
    public void getAccount() throws Exception, RepositoryException {
        new AccountRepository(DummyDatabaseImpl.instance).add(new Account(102));

        Response response = gson.fromJson(requestGet("/account/get?id=102"), Response.class);

        assertNotNull(response);
        assertEquals(0, response.code);
        assertNotNull(response.account);
        assertEquals(102, response.account.getId());
    }

    @Test
    public void cannotGetNonexistentAccount() throws IOException {
        Response response = gson.fromJson(requestGet("/account/get?id=0"), Response.class);

        assertNotNull(response);
        assertEquals(-1, response.code);
        assertNull(response.account);
        assertEquals(response.message, "account not found");
    }
}