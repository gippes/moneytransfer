package controllers;

import db.DummyDatabase;
import db.DummyDatabaseException;
import db.DummyDatabaseImpl;
import model.Account;
import model.Transaction;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.BeforeClass;
import org.junit.Rule;
import services.NettyRESTService;
import services.RequestMapperImpl;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

public class ControllerTest {

    CloseableHttpClient client = HttpClients.createMinimal();

    NettyRESTService restService = new NettyRESTService(8080, new RequestMapperImpl());
    @Rule
    public RestServiceRule rule = new RestServiceRule(client, restService);
    String serviceAddress = "http://localhost:8080";

    @BeforeClass()
    public static void preSetUp() {
        DummyDatabase db = DummyDatabaseImpl.instance;
        try {
            db.addTable(Transaction.ENTITY_NAME, new ConcurrentHashMap<>());
        } catch (DummyDatabaseException ignored) {
        }
        try {
            db.addTable(Account.ENTITY_NAME, new ConcurrentHashMap<>());
        } catch (DummyDatabaseException ignored) {
        }
    }


    public String requestPost(String url, String body) throws IOException {
        HttpPost post = new HttpPost(serviceAddress + url);
        post.setEntity(new StringEntity(body));

        return executeRequest(client.execute(post));
    }

    public String requestGet(String url) throws IOException {
        HttpGet get = new HttpGet(serviceAddress + url);

        return executeRequest(client.execute(get));
    }

    private String executeRequest(CloseableHttpResponse execute) throws IOException {
        String res;
        try (CloseableHttpResponse response = execute) {
            HttpEntity entity = response.getEntity();
            res = entity != null ? IOUtils.toString(entity.getContent()) : "";

        }
        return res;
    }
}
