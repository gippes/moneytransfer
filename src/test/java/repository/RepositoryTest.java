package repository;

import db.DummyDatabase;
import db.DummyDatabaseException;
import db.DummyDatabaseImpl;
import model.Account;
import model.Transaction;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;

import static db.DummyDatabaseImpl.instance;
import static model.Transaction.Status.SUCCESS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RepositoryTest {

    static DummyDatabase db;
    AccountRepository accountRepository;
    TransactionRepository transactionRepository;
    Account account;
    Transaction transaction;

    @BeforeClass
    public static void preSetUp() {
        db = DummyDatabaseImpl.instance;
    }

    @Before
    public void setUp() {
        accountRepository = new AccountRepository(instance);
        transactionRepository = new TransactionRepository(instance);
        account = new Account(100);
        transaction = new Transaction(100, BigDecimal.ZERO, 1, 2);
        try {
            db.addTable(account.getEntityName(), new ConcurrentHashMap<>());
        } catch (DummyDatabaseException ignored) {
        }
        try {
            db.addTable(transaction.getEntityName(), new ConcurrentHashMap<>());
        } catch (DummyDatabaseException ignored) {
        }
    }


    @Test
    public void removeEntity() throws RepositoryException {
        accountRepository.add(account);
        accountRepository.remove(account);
        try {
            accountRepository.getById(account.getId(), account.getEntityName());
        }catch (RepositoryException ignored){
            return;
        }
        fail("account not deleted");

    }

    @Test
    public void updateEntity() throws RepositoryException {
        transactionRepository.add(transaction);
        transaction.status = SUCCESS;
        transactionRepository.update(transaction);
        assertEquals(SUCCESS, transactionRepository.getById(transaction.getId()).status);
    }

}