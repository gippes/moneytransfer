package model;

import org.junit.Test;

import java.math.BigDecimal;

public class AccountTest {

    @Test(expected = IllegalArgumentException.class)
    public void idCannotBeNegative() {
        new Account(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void cannotCreateAccountWithNegativeBalance() {
        new Account(0, new BigDecimal("-1"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void cannotNullBalance() {
       Account account = new Account(0, new BigDecimal("1"));
       account.arrival(null);
    }

}

