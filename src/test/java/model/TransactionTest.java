package model;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class TransactionTest {

    @Test(expected = IllegalArgumentException.class)
    public void idCannotBeNegative(){
       new Transaction(-1,new BigDecimal("0"), 1,2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void amountCannotBeNull(){
        new Transaction(1,null, 1,2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void sourceAccountCannotBeNegative(){
        new Transaction(1,new BigDecimal("1"), -1,2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void targetAccountCannotBeNegative(){
        new Transaction(1,new BigDecimal("1"), 1,-2);
    }
}