import db.DummyDatabaseImpl;
import services.NettyRESTService;
import services.RequestMapperImpl;

public class AppRunner {
    public static void main(String[] args) {
        DummyDatabaseImpl.instance.init();
        int port = 5000;
        if (args.length > 0) port = Integer.parseInt(args[0]);
        new NettyRESTService(port, new RequestMapperImpl()).run();
    }
}
