package repository;

import db.DummyDatabase;
import db.DummyDatabaseImpl;
import model.Transaction;

import static util.Validator.validateArgNotNull;

public class TransactionRepository implements Repository<Transaction> {
    private final DummyDatabase dataBase;

    public TransactionRepository(DummyDatabaseImpl dataBase) {
        validateArgNotNull(dataBase, "dataBase");
        this.dataBase = dataBase;
    }

    @Override
    public DummyDatabase getDatabase() {
        return dataBase;
    }

    public Transaction getById(long id) throws RepositoryException {
        return (Transaction) getById(id, Transaction.ENTITY_NAME);
    }
}
