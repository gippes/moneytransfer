package repository;

public class RepositoryException extends Throwable {
    public RepositoryException(String message){
       super(message);
    }
}
