package repository;

import db.DummyDatabase;
import db.DummyDatabaseException;
import model.Indexable;

import java.util.Map;

import static util.Validator.validateArgNotNull;
import static util.Validator.validateId;

public interface Repository<T extends Indexable> {
    DummyDatabase getDatabase();

    default void add(T object) throws RepositoryException {
        validateArgNotNull(object, object.getEntityName());
        Map<Long, Indexable> table;
        table = getDatabase().getTable(object.getEntityName());
        if (!table.containsKey(object.getId())) {
            table.put(object.getId(), object);
        } else throw new RepositoryException("This " + object.getEntityName() + " already exists");
    }

    default void remove(T object) {
        validateArgNotNull(object, object.getEntityName());
        try {
            getDatabase().getTable(object.getEntityName()).remove(object.getId());
        } catch (DummyDatabaseException ignored) {
        }
    }

    default void update(T object) {
        validateArgNotNull(object, object.getEntityName());
        Map<Long, Indexable> table;
        try {
            table = getDatabase().getTable(object.getEntityName());
        } catch (DummyDatabaseException ignored) {
            return;
        }
        if (table.containsKey(object.getId())) {
            table.remove(object.getId());
            table.put(object.getId(), object);
        }
    }

    default Indexable getById(long id, String tableName) throws RepositoryException {
        validateId(id);
        Map<Long, Indexable> table = getDatabase().getTable(tableName);
        Indexable entity = table.get(id);
        if(entity == null) throw new RepositoryException(tableName + " not found");
        return entity;
    }
}
