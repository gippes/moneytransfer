package repository;

import db.DummyDatabase;
import db.DummyDatabaseImpl;
import model.Account;

import static util.Validator.validateArgNotNull;

public class AccountRepository implements Repository<Account> {
    private final DummyDatabase dataBase;

    public AccountRepository(DummyDatabaseImpl dataBase) {
        validateArgNotNull(dataBase, "dataBase");
        this.dataBase = dataBase;
    }

    @Override
    public DummyDatabase getDatabase() {
        return dataBase;
    }

    public Account getById(long id) throws RepositoryException {
        return (Account) getById(id, Account.ENTITY_NAME);
    }
}
