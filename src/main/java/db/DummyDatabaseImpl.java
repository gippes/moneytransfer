package db;

import model.Account;
import model.Indexable;
import model.Transaction;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static util.Validator.validateArgNotNull;

public enum DummyDatabaseImpl implements DummyDatabase {
    instance;

    public Map<String, Map<Long, Indexable>> tables = new HashMap<>();

    public void init(){
        DummyDatabase db = DummyDatabaseImpl.instance;
        try {
            db.addTable(Transaction.ENTITY_NAME, new ConcurrentHashMap<>());
            db.addTable(Account.ENTITY_NAME, new ConcurrentHashMap<>());
        } catch (DummyDatabaseException ignored) {
        }
    }

    @Override
    public void addTable(String name, ConcurrentHashMap<Long, Indexable> table) throws DummyDatabaseException {
        validateArgNotNull(name, name);
        validateArgNotNull(table, "table");

        if (!tables.containsKey(name)) {
            tables.put(name, table);
        } else throw new DummyDatabaseException("table \"" + name + "\" already created");
    }

    @Override
    public Map<Long, Indexable> getTable(String tableName) throws DummyDatabaseException {
        validateArgNotNull(tableName, "tableName");

        if (tables.containsKey(tableName)) {
            return tables.get(tableName);
        }
        throw new DummyDatabaseException("table \"" + tableName + "\" not found");
    }
}
