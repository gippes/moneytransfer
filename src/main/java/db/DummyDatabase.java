package db;

import model.Indexable;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public interface DummyDatabase {
    void addTable(String name, ConcurrentHashMap<Long, Indexable> table) throws DummyDatabaseException;
    Map<Long, Indexable> getTable(String tableName) throws DummyDatabaseException;
}
