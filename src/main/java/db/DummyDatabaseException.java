package db;

import repository.RepositoryException;

public class DummyDatabaseException extends RepositoryException {
    DummyDatabaseException(String message) {
        super(message);
    }
}
