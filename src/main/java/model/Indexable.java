package model;

public interface Indexable {
    long getId();
    String getEntityName();
}
