package model;

import com.google.gson.annotations.Expose;

import java.math.BigDecimal;

import static model.Transaction.Status.PROCESS;
import static util.Validator.validateArgNotNull;
import static util.Validator.validateId;

public class Transaction implements Indexable {
    public static final String ENTITY_NAME = "transaction";
    @Expose
    private final long id;
    @Expose
    public final BigDecimal amount;
    @Expose
    public final long from;
    @Expose
    public final long to;
    @Expose
    public Status status;


    public Transaction(long id, BigDecimal amount, long from, long to) {
        validateId(id);
        validateId(from);
        validateId(to);
        validateArgNotNull(amount,"amount");
        this.id = id;
        this.amount = amount;
        this.from = from;
        this.to = to;
        status = PROCESS;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transaction that = (Transaction) o;

        if (id != that.id) return false;
        if (from != that.from) return false;
        if (to != that.to) return false;
        if (!amount.equals(that.amount)) return false;
        return status == that.status;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + amount.hashCode();
        result = 31 * result + (int) (from ^ (from >>> 32));
        result = 31 * result + (int) (to ^ (to >>> 32));
        result = 31 * result + status.hashCode();
        return result;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public enum Status{
        PROCESS,
        FAILURE,
        SUCCESS
    }
}
