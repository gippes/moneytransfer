package model;


import com.google.gson.annotations.Expose;

import java.math.BigDecimal;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static util.Validator.validateArgNotNull;
import static util.Validator.validateId;

public class Account implements Indexable {
    public static final String ENTITY_NAME = "account";
    @Expose
    private final long id;
    public Lock lock = new ReentrantLock();
    @Expose
    private BigDecimal balance = BigDecimal.ZERO;

    public Account(long id) {
        validateId(id);
        this.id = id;
    }

    public Account(long id, BigDecimal balance) {
        this(id);
        if (balance.signum() == -1) throw new IllegalArgumentException("cannot create account with negative balance");
        arrival(balance);
    }

    public void expense(BigDecimal amount) {
        validateArgNotNull(amount, null);
        balance = balance.subtract(amount);
    }

    public void arrival(BigDecimal amount) {
        validateArgNotNull(amount, null);
        balance = balance.add(amount);
    }

    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (id != account.id) return false;
        return getBalance().equals(account.getBalance());
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + getBalance().hashCode();
        return result;
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    @Override
    public long getId() {
        return id;
    }

}
