package services;

import controllers.AccountController;
import controllers.EchoController;
import controllers.TransactionController;
import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.HttpResponseStatus;

import static services.NettyRESTService.HttpResponse;

public class RequestMapperImpl implements RequestMapper {

    @Override
    public HttpResponse processingController(String uri, ByteBuf content) {
        String[] segments = uri.split("/", 3);
        if (segments.length > 1) {
            String queryData = segments.length > 2 ? segments[2] : null;

            switch (segments[1]) {
                case "echo":
                    return new EchoController().query(queryData, content);
                case "transaction":
                    return new TransactionController().query(queryData, content);
                case "account":
                    return new AccountController().query(queryData,content);
            }

        }
        return new HttpResponse(HttpResponseStatus.NOT_FOUND);
    }
}
