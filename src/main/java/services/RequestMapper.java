package services;

import io.netty.buffer.ByteBuf;
import services.NettyRESTService.HttpResponse;

interface RequestMapper {
    HttpResponse processingController(String uri, ByteBuf content);
}

