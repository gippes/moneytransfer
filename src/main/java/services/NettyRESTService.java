package services;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.util.concurrent.CountDownLatch;

import static io.netty.channel.ChannelFutureListener.CLOSE;
import static io.netty.handler.codec.http.HttpResponseStatus.INTERNAL_SERVER_ERROR;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;
import static io.netty.util.CharsetUtil.UTF_8;
import static java.util.concurrent.TimeUnit.SECONDS;
import static util.Validator.validateArgNotNull;

public class NettyRESTService implements Runnable, Closeable {
    private static Logger log = LoggerFactory.getLogger(NettyRESTService.class);
    private final int port;
    private final RequestMapper requestMapper;
    private ChannelFuture channelFuture;
    private CountDownLatch initAwait = new CountDownLatch(1);

    public NettyRESTService(int port, RequestMapper requestMapper) {
        validateArgNotNull(requestMapper, "requestMapper");
        this.port = port;
        this.requestMapper = requestMapper;
    }

    public void run() {
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        try {
            ServerBootstrap server = new ServerBootstrap()
                    .group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) {
                            ch.pipeline()
                                    .addLast("encoder", new HttpResponseEncoder())
                                    .addLast("decoder", new HttpRequestDecoder())
                                    .addLast("aggregator", new HttpObjectAggregator(Integer.MAX_VALUE))
                                    .addLast("worker", new WorkerHandler(requestMapper));
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 100)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);

            channelFuture = server.bind("localhost", port).sync();
            initAwait.countDown();
            channelFuture.channel().closeFuture().sync();

        } catch (Exception e) {
            log.error("service emergency stopped. Reason: {}", e.getMessage());

        } finally {
            workerGroup.shutdownGracefully();
            if (channelFuture != null) channelFuture.channel().close().awaitUninterruptibly();
        }
    }

    public void awaitStartService() {
        try {
            initAwait.await(5, SECONDS);
        } catch (InterruptedException ignored) {
            log.warn("timed out waiting for the service to start");
        }
    }

    @Override
    public void close() {
        if (channelFuture != null) channelFuture.channel().close();
    }

    private static class WorkerHandler extends ChannelInboundHandlerAdapter {
        private RequestMapper requestMapper;

        WorkerHandler(RequestMapper requestMapper) {
            this.requestMapper = requestMapper;
        }

        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) {
            FullHttpRequest request = (FullHttpRequest) msg;
            DefaultFullHttpResponse response;
            try {
                HttpResponse res = requestMapper.processingController(request.uri(), request.content());
                if (request.content() != null) response = new DefaultFullHttpResponse(HTTP_1_1, res.status, res.content);
                else response = new DefaultFullHttpResponse(HTTP_1_1, OK);
            } catch (Exception e) {
                response = new DefaultFullHttpResponse(HTTP_1_1, INTERNAL_SERVER_ERROR);
            }

            ctx.writeAndFlush(response).addListener(CLOSE);
            request.release();
        }
    }

    public static class HttpResponse {
        HttpResponseStatus status;
        ByteBuf content = null;

        public HttpResponse(HttpResponseStatus status) {
            validateArgNotNull(status, "status");
            this.status = status;
        }

        public HttpResponse(HttpResponseStatus status, String content) {
            this(status);
            if (content != null) this.content = Unpooled.copiedBuffer(content, UTF_8);
        }
    }
}