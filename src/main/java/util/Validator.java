package util;

public class Validator {
    public static void validateId(long id){
       if(id < 0) throw new IllegalArgumentException("id cannot be negative");
    }

    public static void validateArgNotNull(Object obj, String argName){
        if(obj == null) throw new IllegalArgumentException(argName + " cannot be null");
    }
}
