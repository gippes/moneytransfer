package controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import db.DummyDatabaseImpl;
import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.QueryStringDecoder;
import model.Account;
import repository.AccountRepository;
import repository.RepositoryException;

import java.util.List;
import java.util.Map;

import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.handler.codec.rtsp.RtspResponseStatuses.BAD_REQUEST;
import static io.netty.handler.codec.rtsp.RtspResponseStatuses.METHOD_NOT_VALID;
import static io.netty.util.CharsetUtil.UTF_8;
import static services.NettyRESTService.HttpResponse;

public class AccountController implements Controller {

    @Override
    public HttpResponse query(String queryData, ByteBuf content) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        QueryStringDecoder query = new QueryStringDecoder(queryData, true);

        try {
            AccountRepository repository = new AccountRepository(DummyDatabaseImpl.instance);
            switch (query.path()) {
                case "create":
                    Account account = gson.fromJson(content.toString(UTF_8), Account.class);
                    if (account != null) {
                        repository.add(account);
                        return new HttpResponse(OK, gson.toJson(new Response(0, "account id:" + account.getId() + " created", null)));
                    }
                case "get":
                    Map<String, List<String>> params = query.parameters();
                    if (params != null) {
                        if (params.containsKey("id")) {
                            long id = Long.parseLong(params.get("id").get(0));
                            return new HttpResponse(OK,gson.toJson(new Response(0,"success", repository.getById(id))));
                        }
                    }
            }
        } catch (RepositoryException e) {
            return new HttpResponse(BAD_REQUEST, gson.toJson(new Response(-1, e.getLocalizedMessage(), null)));
        }
        return new HttpResponse(METHOD_NOT_VALID, gson.toJson(new Response(-1, "bad request", null)));
    }

    public static class Response {
        @Expose
        int code;
        @Expose
        String message;
        @Expose
        Account account;

        public Response(int code, String message, Account account) {
            this.code = code;
            this.message = message;
            this.account = account;
        }
    }
}
