package controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import db.DummyDatabaseImpl;
import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.QueryStringDecoder;
import model.Account;
import model.Transaction;
import repository.AccountRepository;
import repository.RepositoryException;
import repository.TransactionRepository;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static io.netty.handler.codec.rtsp.RtspResponseStatuses.*;
import static io.netty.util.CharsetUtil.UTF_8;
import static model.Transaction.Status.FAILURE;
import static model.Transaction.Status.PROCESS;
import static services.NettyRESTService.HttpResponse;

public class TransactionController implements Controller {

    @Override
    public HttpResponse query(String queryData, ByteBuf content) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        QueryStringDecoder query = new QueryStringDecoder(queryData, true);
        TransactionRepository repository = new TransactionRepository(DummyDatabaseImpl.instance);
        switch (query.path()) {
            case "execute":
                Transaction trn = gson.fromJson(content.toString(UTF_8), Transaction.class);
                try {
                    trn.status = PROCESS;
                    repository.add(trn);
                    executeTransaction(trn);
                } catch (RepositoryException e) {
                    trn.status = FAILURE;
                    repository.update(trn);
                    return new HttpResponse(BAD_REQUEST, gson.toJson(new Response(-1, e.getLocalizedMessage(), null)));
                }
                return new HttpResponse(OK, gson.toJson(new Response(0, "transaction id:" + trn.getId() + " executed successful", trn)));

            case "get":
                Map<String, List<String>> params = query.parameters();
                if (params != null) {
                    if (params.containsKey("id")) {
                        long id = Long.parseLong(params.get("id").get(0));
                        try {
                            return new HttpResponse(OK, gson.toJson(new Response(0, "transaction id:" + id + " info", repository.getById(id))));
                        } catch (RepositoryException e) {
                            return new HttpResponse(BAD_REQUEST, gson.toJson(new Response(-1, e.getLocalizedMessage(), null)));
                        }
                    }
                }
        }

        return new HttpResponse(METHOD_NOT_VALID, gson.toJson(new Response(-1, "bad request", null)));
    }

    public void executeTransaction(Transaction trn) throws RepositoryException {
        AccountRepository repository = new AccountRepository(DummyDatabaseImpl.instance);
        Account source;
        Account target;
        try {
            source = repository.getById(trn.from);
            target = repository.getById(trn.to);
            int attempt = 5;
            while (true) {
                try {
                    if (source.lock.tryLock(100, TimeUnit.MILLISECONDS)) {
                        if (target.lock.tryLock(100, TimeUnit.MILLISECONDS)) {
                            source.expense(trn.amount);
                            target.arrival(trn.amount);
                            break;
                        }
                    }
                } catch (InterruptedException ignored) {
                } finally {
                    target.lock.unlock();
                    source.lock.unlock();
                }
                attempt--;
                if (attempt == 0) throw new RepositoryException("one of the two accounts is unavailable");
            }
        } catch (RepositoryException e) {
            throw new RepositoryException(e.getMessage());
        }

    }

    public static class Response {
        @Expose
        public int code;
        @Expose
        public String message;
        @Expose
        public Transaction transaction;

        public Response(int code, String message, Transaction transaction) {
            this.code = code;
            this.message = message;
            this.transaction = transaction;
        }
    }
}
