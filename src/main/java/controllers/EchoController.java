package controllers;

import io.netty.buffer.ByteBuf;

import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.util.CharsetUtil.UTF_8;
import static services.NettyRESTService.HttpResponse;

public class EchoController implements Controller {
    @Override
    public HttpResponse query(String queryData, ByteBuf content) {
        return new HttpResponse(OK, content.toString(UTF_8));
    }
}
