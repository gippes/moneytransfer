package controllers;


import io.netty.buffer.ByteBuf;
import services.NettyRESTService.HttpResponse;

public interface Controller {
    HttpResponse query(String queryData, ByteBuf content);
}
