#Money transfer between accounts

##How to start

to install use command
./gradlew fatjar 

to run use command
java -jar ./build/libs/moneytransfer-1.0.jar

##API example

###Create account

request:

POST /account/create
{
  "id":101,
  "balance":0
}

response:

HTTP 200
{
  "code":0,
  "message":"account id:101 created"
} 

####Get account

request:

GET /account/get?id=102

response:

HTTP 200:
{
  "code":0,
  "message":"success",
  "account":{
    "id":102,
    "balance":0
  }
}

####Execute transaction

request:

POST /transaction/execute
{
  "id":1,
  "amount":10,
  "from":1,
  "to":2,
}

response:

HTTP 200:
{
  "code":0,
  "message":"transaction id:1 executed successful",
  "transaction":{
    "id":1,
    "amount":10,
    "from":1,
    "to":2,
    "status":"PROCESS"
  }
}

####Get transaction

request:

GET /transaction/get?id=5

response:

HTTP 200:
{
  "code":0,
  "message":"transaction id:5 info",
  "transaction":{
    "id":5,
    "amount":0,
    "from":1,
    "to":3,
    "status":"PROCESS"
  }
}
